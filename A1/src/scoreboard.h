/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "shared.h"
#include "player.h"

#ifndef SCOREBOARD_H
#define SCOREBOARD_H

/*Scoreboard array size*/
#define MAX_SCORES 10

/*Creating a type from the player structure*/
typedef struct player score;

/*Function prototypes.*/
void init_scoreboard (score scores[MAX_SCORES]);
BOOLEAN add_to_scoreboard (score scores[MAX_SCORES], struct player *winner);
void display_scores (score scores[MAX_SCORES]);

#endif /* ifndef SCOREBOARD_H */