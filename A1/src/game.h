/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include <time.h>
#include "shared.h"
#include "player.h"
#include "gameboard.h"

#ifndef GAME_H
#define GAME_H

/*Direction definitions*/
#define DIRECTIONS 8
enum direction
{
    NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST
};

/*Turn input variables*/
#define TURN_LEN 3
#define TURN_MIN 1
#define TURN_MAX 8

/*Function prototypes*/
struct player *play_game (struct player *first, struct player *second);
BOOLEAN make_move (struct player *player, game_board board);
BOOLEAN apply_move (game_board board, int x, int y, enum cell player_token);
unsigned game_score (game_board board, enum cell player_token);
void swap_players (struct player **first, struct player **second);

#endif /* ifndef GAME_H */