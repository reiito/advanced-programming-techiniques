/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include "gameboard.h"
#include "player.h"

/*Initializes the game board to be filled with blank spaces
exept for the the 4 middle slots which are fillled with 2 red and blue toekns*/
void init_game_board(game_board board)
{
	int row, col;
   
   for (row = 0; row < BOARD_HEIGHT; row++)
   {
      for (col = 0; col < BOARD_WIDTH; col++)
      {
         board[row][col] = BLANK;
      }
   }
}

/*Displays the game board by first displaying both player's details
then checking the board array for what's supposed to be in there and changing its color.*/
void display_board(game_board board, struct player *first, struct player *second)
{
   enum cell *redA, *redB, *blueA, *blueB;
	int row, col;
   
   redA = &board[3][3];
   redB = &board[4][4];
   blueA = &board[3][4];
   blueB = &board[4][3];
   
   printEquals();
   printPlayer(*first, PLAYER_ONE);
   printPlayer(*second, PLAYER_TWO);
   
	printf("     1   2   3   4   5   6   7   8  \n");
   printEquals();
   for (row = 0; row < BOARD_HEIGHT; row++)
   {
      printf(" %d |", row+1);
      for (col = 0; col < BOARD_WIDTH; col++)
      {
         if (&board[row][col] == redA || &board[row][col] == redB)
         {
            board[row][col] = RED;
            printf(" %s0%s |", COLOR_RED, COLOR_RESET);
         }
         else if (&board[row][col] == blueA || &board[row][col] == blueB)
         {
            board[row][col] = BLUE;
            printf(" %s0%s |", COLOR_BLUE, COLOR_RESET);
         }
         else
         {
            printf("   |");
         }
      }
      printf("\n");
      printDashes();
   }
}