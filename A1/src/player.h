/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include "shared.h"
#include "gameboard.h"
#include "utility.h"

#ifndef PLAYER_H
#define PLAYER_H

/*Mximum length of a player name */
#define NAME_LEN 20

/*Avoiding magic numbers*/
#define PLAYER_ONE 1
#define PLAYER_TWO 2

/*Player structure declaration*/
struct player
{
    char name[NAME_LEN + 1];
    enum cell token;
    unsigned score;
};

/*Function prototypes*/
void printPlayer(struct player player, int playerOrder);
BOOLEAN init_first_player (struct player *first, enum cell *token);
BOOLEAN init_second_player (struct player *second, enum cell token);

#endif /* ifndef PLAYER_H */