 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_utility.h"
/**
 * @file ppd_utility.c contains implementations of important functions for
 * the system. If you are not sure where to implement a new function, 
 * here is probably a good spot.
 **/

void read_rest_of_line(void)
{
    int ch;
    /* keep retrieving characters from stdin until we
     * are at the end of the buffer
     */
    while(ch = getc(stdin), ch!='\n' && ch != EOF)
        ;
    /* reset error flags on stdin */
    clearerr(stdin);
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
BOOLEAN system_init(System *system)
{
   int i;
   unsigned coinType[NUM_DENOMS] = { 5, 10, 20, 50, 100, 200, 500, 1000 };
   
   system->item_list = malloc(sizeof(System));
   
   for (i = 0; i < NUM_DENOMS; i++)
   {
      system->cash_register[i].denom = coinType[i];
      system->cash_register[i].count = 0;
   }
   
   system->item_list->head = NULL;
   system->item_list->count = 0;
   
   system->coin_file_name = NULL;
   system->stock_file_name = NULL;
   
   return TRUE;
}

/**
 * loads the stock file's data into the system. This needs to be 
 * implemented as part of requirement 2 of the assignment specification.
 **/
BOOLEAN load_stock(System *system, const char *filename)
{
   FILE *stockFile;
   char stockLine[STOCK_LINE_LEN];
   char *token, *endPtr, *price, *dollars, *cents, *onHand;
   Item *newItem = malloc(sizeof(Item));
   
   stockFile = fopen(filename, "r");
   
   /* check if file exists */
   if (stockFile == NULL)
   {
      printf("[ERROR] The entry for the stock file is empty. \n");
      free(newItem);
      return FALSE;
   }
   
   /* check if file contents are emtpy */
   fseek(stockFile, 0, SEEK_END);
   if (ftell(stockFile) == 0)
   {
      printf("[ERROR] File: '%s' is empty. \n", filename);
      free(newItem);
      return FALSE;
   }
   fseek(stockFile, 0, SEEK_SET);
   
   system->stock_file_name = filename;
   
   while (fgets(stockLine, sizeof(stockLine), stockFile) != NULL)
   {
      /* id */
      token = strtok(stockLine, "|");
      strcpy(newItem->id, token);
      
      /* name */
      token = strtok(NULL, "|");
      strcpy(newItem->name, token);
      
      /* description */
      token = strtok(NULL, "|");
      strcpy(newItem->desc, token);
      
      /* price */
      token = strtok(NULL, "|");
      price = token;
      
      /* on hand */
      token = strtok(NULL, "\n");
      onHand = token;
      
      /* check if fields are empty */
      if (token == NULL || strcmp(token, "\n") == 0)
      {
         printf("[ERROR] In file: %s, field(s) are mssing. \n", filename);
         free(newItem);
         return FALSE;
      }
      
      /* price struct */
      dollars = strtok(price, ".");
      cents = strtok(NULL, ".");
      
      newItem->price.dollars = strtoul(dollars, &endPtr, 10);
      newItem->price.cents = strtoul(cents, &endPtr, 10);
      
      /* check if fields are valid */
      if (idExists(system, newItem->id) || !validID(newItem->id) || !validName(newItem->name) || !validDesc(newItem->desc) || !validPrice(newItem->price) || !validOnHand(onHand))
      {
         printf("[ERROR] Item: %s(%s) from file: '%s', has invalid values. \n", newItem->name, newItem->id, filename);
         free(newItem);
         return FALSE;
      }
      
      newItem->on_hand = strtoul(onHand, &endPtr, 10);
      
      if (!addToStockList(system, newItem))
      {
         printf("[ERROR] Unable to add item to list.");
         free(newItem);
         return FALSE;
      }
   }
   
   fclose(stockFile);
   
   free(newItem);
   return TRUE;
}

/**
 * loads the contents of the coins file into the system. This needs to
 * be implemented as part 1 of requirement 18.
 **/
BOOLEAN load_coins(System *system, const char *filename)
{
   FILE *coinsFile;
   char coinLine[COIN_LINE_LEN];
   char *token, *endPtr, *denom, *qty;
   int denomCount, i = NUM_DENOMS - 1;
   
   coinsFile = fopen(filename, "r");
   
   /* check if file exists */
   if (coinsFile == NULL)
   {
      printf("[ERROR] The entry for the 'coins' file is empty. \n");
      return FALSE;
   }
   
   /* check if file contents are emtpy */
   fseek(coinsFile, 0, SEEK_END);
   if (ftell(coinsFile) == 0)
   {
      printf("[ERROR] File: '%s' is empty. \n", filename);
      return FALSE;
   }
   fseek(coinsFile, 0, SEEK_SET);
   
   system->coin_file_name = filename;
   
   while (fgets(coinLine, sizeof(coinLine), coinsFile) != NULL)
   {
      /* denomination */
      token = strtok(coinLine, ",");
      denom = token;
      
      /* quantity */
      token = strtok(NULL, "\n");
      qty = token;
      
      /* check if fields are empty */
      if (token == NULL || strcmp(token, "\n") == 0)
      {
         printf("[ERROR] In file: %s, field(s) are mssing. \n", filename);
         return FALSE;
      }
      
      /* check if fields are valid */
      if (!validDenom(denom) || !validQty(qty))
      {
         printf("[ERROR] In file: %s, field(s) are invalid. \n", filename);
         return FALSE;
      }
      
      system->cash_register[i].denom = strtoul(denom, &endPtr, 10);
      system->cash_register[i].count = strtoul(qty, &endPtr, 10);
      
      denomCount++;
      i--;
   }
   
   /* check if amount of denominations is valid */
   if(denomCount != NUM_DENOMS)
   {
      printf("[ERROR] The amount of denominations is wrong. \n");
      return FALSE;
   }
   
   fclose(coinsFile);
   
   return TRUE;
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
void system_free(System *system)
{
   Node *node = system->item_list->head;
   
   while(node != NULL)
   {
      Node *temp = node;
      node = node->next;

      free(temp->data);
      free(temp);
   }
   
   free(system->item_list);
}

/* converts a given string into an int with max and min values */
int convStrToInt(char *string, int min, int max)
{
   int integer;
   char *strEndPtr;
   
   integer = (int)strtol(string, &strEndPtr, 10);
   if (strcmp(strEndPtr, "") != 0)
   {
      printf("[INVALID] Input must be numeric. \n");
      return FALSE;
   }
   else if (integer < min || integer > max)
   {
      printf("[INVALID] Input must be between: %d and %d. \n", min, max);
      return FALSE;
   }
   
   return integer;
}

/* validates a user inputted string */
BOOLEAN validInpStr(char *prompt, char *usrString, unsigned inpLen)
{
   BOOLEAN inpChecked = FALSE;
   char input[MAX_LINE_LEN + EXTRA_SPACES];
   int strLength;
   char *usrInp;
   
   while (!inpChecked)
   {
      printf("%s", prompt);
      
      usrInp = fgets(input, inpLen + EXTRA_SPACES, stdin);
      strLength = strlen(input);
   
      if (usrInp == NULL)
      {
         printf("\n");
         return FALSE;
      }
      if (strcmp(usrInp, "\n") == 0)
      {
         input[strLength - 1] = '\0';
         return FALSE;
      }
      
      if(input[strLength - 1] != '\n')
      {
         printf("[INVALID] Input was too long. \n");
         read_rest_of_line();
      }
      else
      {
         inpChecked = TRUE;
      }
   }
   input[strLength - 1] = '\0';
   strcpy(usrString, input);
   return TRUE;
}