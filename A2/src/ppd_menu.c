 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_menu.h"
/**
 * @file ppd_menu.c handles the initialised and management of the menu 
 * array
 **/

/**
 * @param menu the menu item array to initialise
 **/
void init_menu(struct menu_item *menu)
{
   struct menu_item newMenu[] = 
   { 
      { "Display Items",  display_items }, 
      { "Purchase Items", purchase_item },
      { "Save and Exit",  save_system },
      { "Add Item",       add_item },
      { "Remove Item",    remove_item },
      { "Display Coins",  display_coins },
      { "Reset Stock",    reset_stock },
      { "Reset Coins",    reset_coins },
      { "Abort Program",  abort_program }
   };
   
   memcpy(menu, newMenu, sizeof(newMenu));
}

/**
 * @return a menu_function that defines how to perform the user's
 * selection
 **/
menu_function get_menu_choice(struct menu_item *menu)
{
   int i;
   int menuChoice;
  
   /* Print user menu */
   printf("Main Menu: \n");
   for (i = 0; i < NUM_USER_ITEMS; i++)
   {
      printf("%d. %s \n", i+1, menu[i].name);
   }
   /* Print admin menu */
   printf("Administrator-Only Menu: \n");
   for (i = NUM_USER_ITEMS; i < NUM_MENU_ITEMS; i++)
   {
      printf("%d. %s \n", i+1, menu[i].name);
   }
   
   menuChoice = validMenuInt(CHOICE_LEN, CHOICE_MIN, CHOICE_MAX);
   
   return menu[menuChoice - 1].function;
}

/* validates the user's menu input */
int validMenuInt(unsigned inpLen, int min, int max)
{
   BOOLEAN inpChecked = FALSE;
   char input[CHOICE_LEN + EXTRA_SPACES];
   char *menuPrompt = "Select your option (1-9): ";
   int menuInt;
   
   while (inpChecked == FALSE)
   {
      if (validInpStr(menuPrompt, input, inpLen))
      {
         menuInt = convStrToInt(input, min, max);
      
         if (menuInt != FALSE)
         {
            inpChecked = TRUE;
         }
      }
   }
   
   return menuInt;
}